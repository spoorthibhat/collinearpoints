import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

/**---------------------------------------------------------------------------
 * Author: Spoorthi Bhat
 * Written: 10/20/2017
 * Purpose: To write a faster program that examines a point origin and checks
 * whether this origin participates in a set of 4 or more collinear points.
 * Applying this method for each of the n points yields an effecetive algorithm
 * to the problem.
 ---------------------------------------------------------------------------*/
public class FastCollinearPoints {
    /**
     * line segments containing 4 or more collinear points.
     */
    private final List<LineSegment> lineSegments;

    /**
     * Constructor that finds all line segments containing 4 or more points.
     * @param points - input points for which line segment with
     *               collinear points is to be founded.
     */
    public FastCollinearPoints(Point[] points) {
        validate(points);
        lineSegments = new ArrayList<LineSegment>();

        // Creating a copy of original array
        Point[] auxillaryArray = new Point[points.length];

        ArrayList<Point> startingPoint = new ArrayList<Point>();
        ArrayList<Point> endPoint = new ArrayList<Point>();


        for (int index = 0; index < auxillaryArray.length; index++) {
            auxillaryArray[index] = points[index];
        }

        for (int origin = 0; origin < points.length; origin++) {

            Point originPoint = points[origin];
            Arrays.sort(auxillaryArray, originPoint.slopeOrder());

            ArrayList<Point> tempPointList = new ArrayList<Point>();

            double prevSlope = Double.NEGATIVE_INFINITY;
            int count = 0;

            for (int i = 1; i < auxillaryArray.length; i++) {

                double currSlope = originPoint.slopeTo(auxillaryArray[i]);
                if (tempPointList.isEmpty()) {
                    tempPointList.add(auxillaryArray[i]);
                    count++;
                } else if (Double.compare(currSlope, prevSlope) == 0) {
                    tempPointList.add(auxillaryArray[i]);
                    count++;
                    if (count >= 3 && i == auxillaryArray.length - 1) {
                        if (originPoint.compareTo(tempPointList.get(0)) == -1) {
                            startingPoint.add(originPoint);
                            endPoint.add(tempPointList.get(count - 1));
                        } else if (originPoint.compareTo(
                                tempPointList.get(count - 1)) == +1) {
                            startingPoint.add(tempPointList.get(0));
                            endPoint.add(originPoint);
                        } else {
                            startingPoint.add(tempPointList.get(0));
                            endPoint.add(tempPointList.get(count - 1));
                        }
                    }
                } else {
                    if (count >= 3) {
                        // Avoiding subsegments
                        if (originPoint.compareTo(tempPointList.get(0)) == -1) {
                            startingPoint.add(originPoint);
                            endPoint.add(tempPointList.get(count - 1));
                            } else if (originPoint.compareTo(
                                tempPointList.get(count - 1)) == +1) {
                            startingPoint.add(tempPointList.get(0));
                            endPoint.add(originPoint);
                        } else {
                            startingPoint.add(tempPointList.get(0));
                            endPoint.add(tempPointList.get(count - 1));
                        }
                    }
                    tempPointList.clear();
                    count = 1;
                    tempPointList.add(auxillaryArray[i]);
                }
                prevSlope = currSlope;
            }
        }
        findUniqueLines(startingPoint, endPoint);

    }

    /**
     * Method to remove duplicates.
     * @param start list storing the start point of line segment
     * @param end list storing the start point of line segment
     */
    private void findUniqueLines(ArrayList<Point> start, ArrayList<Point> end) {
        boolean[] isRepeat = new boolean[start.size()]; // Defaulted to false.
        for (int i = 0; i < start.size(); i++) {
            for (int j = i + 1; j < start.size(); j++) {
                if (start.get(i).compareTo(start.get(j)) == 0
                        && end.get(i).compareTo(end.get(j)) == 0) {
                    isRepeat[j] = true;
                }
            }
        }

        for (int i = 0; i < start.size(); i++) {
            if (!isRepeat[i]) {
                lineSegments.add(new LineSegment(start.get(i), end.get(i)));
            }
        }
    }

    /**
     * A method to validate the array points.
     * @param points array under validation.
     * @throws IllegalArgumentException if array is null or if one of the
     * points is null or if the array contains duplicates.
     */
    private void validate(Point[] points) {
        if (points == null) {
            throw new IllegalArgumentException("Points array was null.");
        } else {
            for (int index = 0; index < points.length - 1; index++) {
                Point firstPoint = points[index];
                if (firstPoint == null) {
                    throw new IllegalArgumentException("One of the "
                            + "points in the array is null.");
                }
                for (int indexSecond = index + 1; indexSecond < points.length;
                     indexSecond++) {
                    if (firstPoint.compareTo(points[indexSecond]) == 0) {
                        throw new IllegalArgumentException("Two points"
                                + " in the array are the same.");
                    }
                }
            }
        }
    }

    /**
     * The total number of line segements.
     * @return the count of segements.
     */
    public int numberOfSegments() {
        // the number of line segments
        return lineSegments.size();
    }

    /**
     *  A method to provide the line segments.
     * @return the line segment array.
     */
    public LineSegment[] segments() {

        return lineSegments.toArray(new LineSegment[lineSegments.size()]);
    }

    /**
     *
     * @param args args[0] gives the input file.
     */
    public static void main(String[] args) {
        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();

    }
}

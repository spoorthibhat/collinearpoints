import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

/**---------------------------------------------------------------------------
 * Author: Spoorthi Bhat
 * Written: 10/20/2017
 * Purpose: To write a bruteforce program that examines 4 points at a time
 * and checks whether they all lie on the same line segment, returning all
 * such line segments.
 ---------------------------------------------------------------------------*/
public class BruteCollinearPoints {

    /**
     * line segments containing 4 collinear points.
     */
    private final List<LineSegment> lineSegments;

    /**
     * Constructor that finds all line segments containing 4 points.
     * @param points - input array of points.
     */
    public BruteCollinearPoints(Point[] points) {
        validate(points);
        lineSegments = new ArrayList<LineSegment>();
        ArrayList<Point> startingPoint = new ArrayList<Point>();
        ArrayList<Point> endPoint = new ArrayList<Point>();


        for  (int i = 0; i < points.length - 3; i++) {
            for (int j = i + 1; j < points.length - 2; j++) {
                for (int k = j + 1; k < points.length - 1; k++) {

                    double slope1 = points[i].slopeTo(points[j]);
                    double slope2 = points[i].slopeTo(points[k]);
                    if (Double.compare(slope1, slope2) != 0) {
                        break;
                    }
                    for (int s = k + 1; s < points.length; s++) {

                        double slope3 = points[i].slopeTo(points[s]);
                        if (Double.compare(slope2, slope3) == 0) {
                            // adding the collinear points into auxillary array
                            Point[] auxilaryPoints  = new Point[4];
                            auxilaryPoints[0] = points[i];
                            auxilaryPoints[1] = points[j];
                            auxilaryPoints[2] = points[k];
                            auxilaryPoints[3] = points[s];

                            // Sorting the array to get the end points
                            // of the line segment and avoiding sub segments
                            Arrays.sort(auxilaryPoints);
                            startingPoint.add(auxilaryPoints[0]);
                            endPoint.add(auxilaryPoints[3]);
                            }
                        }
                    }
                }
            }
        findUniqueLines(startingPoint, endPoint);
        }



    /**
     * Method to remove duplicates.
     * @param start list storing the start point of line segment
     * @param end list storing the start point of line segment
     */
    private void findUniqueLines(ArrayList<Point> start, ArrayList<Point> end) {
        boolean[] isRepeat = new boolean[start.size()]; // Defaulted to false.
        for (int i = 0; i < start.size(); i++) {
            for (int j = i + 1; j < start.size(); j++) {
                if (start.get(i).compareTo(start.get(j)) == 0
                        && end.get(i).compareTo(end.get(j)) == 0) {
                    isRepeat[j] = true;
                }
            }
        }

        for (int i = 0; i < start.size(); i++) {
            if (!isRepeat[i]) {
                lineSegments.add(new LineSegment(start.get(i), end.get(i)));
            }
        }
    }
    /**
     * A method to validate the array points.
     * @param points array under validation.
     * @throws IllegalArgumentException if array is null or if one of the
     * points is null or if the array contains duplicates.
     */
    private void validate(final Point[] points) {
        if (points == null) {
            throw new IllegalArgumentException("Points array was null.");
        } else {
            for (int index = 0; index < points.length - 1; index++) {
                Point firstPoint = points[index];
                if (firstPoint == null) {
                    throw new IllegalArgumentException("One of "
                            + "the points in the array is null.");
                }
                for (int indexSecond = index + 1; indexSecond
                        < points.length; indexSecond++) {
                    if (firstPoint.compareTo(points[indexSecond]) == 0) {
                        throw new IllegalArgumentException("Two points"
                                + " in the array are the same.");
                    }
                }
            }
        }
    }



    /**
     * The total number of line segements.
     * @return the count of segements.
     */
    public int numberOfSegments() {
        return lineSegments.size();
    }

    /**
     *  A method to provide the line segments.
     * @return the line segment array.
     */
    public LineSegment[] segments() {
        return lineSegments.toArray(new LineSegment[lineSegments.size()]);
    }

    /**
     *
     * @param args arg[0] gives the input file.
     */
    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}

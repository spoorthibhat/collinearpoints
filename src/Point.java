
import java.util.Comparator;
import edu.princeton.cs.algs4.StdDraw;

/**---------------------------------------------------------------------------
 * Author: Spoorthi Bhat
 * Written: 10/20/2017
 * Purpose: To create an immutable data type Point that represents a point
 * in the plane.
 ---------------------------------------------------------------------------*/
public class Point implements Comparable<Point> {

    private final int x;     // x-coordinate of this point
    private final int y;     // y-coordinate of this point

    /**
     * Initializes a new point.
     *
     * @param  x the <em>x</em>-coordinate of the point
     * @param  y the <em>y</em>-coordinate of the point
     */
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    /**
     * Draws this point to standard draw.
     */
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    /**
     * Draws the line segment between this point and the specified point
     * to standard draw.
     *
     * @param that the other point
     */
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    /**
     * Returns the slope between this point and the specified point.
     * Formally, if the two points are (x0, y0) and (x1, y1), then the slope
     * is (y1 - y0) / (x1 - x0). For completeness, the slope is defined to be
     * +0.0 if the line segment connecting the two points is horizontal;
     * Double.POSITIVE_INFINITY if the line segment is vertical;
     * and Double.NEGATIVE_INFINITY if (x0, y0) and (x1, y1) are equal.
     *
     * @param  that the other point
     * @return the slope between this point and the specified point
     */
    public double slopeTo(Point that) {
        if (this.y == that.y) {
            if (this.x == that.x) {
                return Double.NEGATIVE_INFINITY; // degenerate line segment
            }
            return +0; // horizontal line
        } else if (this.x == that.x) {
            return Double.POSITIVE_INFINITY; // vertical line
        } else {
            return (double) (that.y - this.y) / (that.x - this.x);
        }
    }

    /**
     * Compares two points by y-coordinate, breaking ties by x-coordinate.
     * Formally, the invoking point (x0, y0) is less than the argument point
     * (x1, y1) if and only if either y0 < y1 or if y0 = y1 and x0 < x1.
     *
     * @param  that the other point
     * @return the value <tt>0</tt> if this point is equal to the argument
     *         point (x0 = x1 and y0 = y1);
     *         a negative integer if this point is less than the argument
     *         point; and a positive integer if this point is greater than the
     *         argument point
     */
    public int compareTo(Point that) {
        /* YOUR CODE HERE */
        if (this.x == that.x && this.y == that.y) {
            return 0;
        } else if (this.y < that.y || (this.y == that.y && this.x < that.x)) {
            return -1;
        } else {
            return +1;
        }
    }

    /**
     * Compares two points by the slope they make with this point.
     * The slope is defined as in the slopeTo() method.
     *
     * @return the Comparator that defines this ordering on points
     */
    public Comparator<Point> slopeOrder() {
        /* YOUR CODE HERE */
        return new SlopeComparator();
    }

    /**
     * A private class that implements the comparator.
     */
    private class SlopeComparator implements Comparator<Point> {

        public int compare(Point first, Point second) {

            double slope1 = slopeTo(first);
            double slope2 = slopeTo(second);

            if (Double.compare(slope1, slope2) == 0) {
                return first.compareTo(second);
            } else if (Double.compare(slope1, slope2) > 0) {
                return +1;
            } else {
                return -1;
            }
        }
    }

    /**
     * Returns a string representation of this point.
     * This method is provide for debugging;
     * your program should not rely on the format of the string representation.
     *
     * @return a string representation of this point
     */
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }

    /**
     * Unit tests the Point data type.
     */
    /* public static void main(String[] args) {
        *//* YOUR CODE HERE *//*
        Point[] pointss = new Point[8];
        pointss[0] = new Point(10000, 0);
        pointss[1] = new Point(0, 10000);
        pointss[2] = new Point(3000, 7000);
        pointss[3] = new Point(7000, 3000);
        pointss[4] = new Point(20000, 21000);
        pointss[5] = new Point(3000, 4000);
        pointss[6] = new Point(14000, 15000);
        pointss[7] = new Point(6000, 7000);

        Arrays.sort(pointss);
        for (int i=0 ; i<pointss.length; i++){
            System.out.println(pointss[i].toString());
        }
        Arrays.sort(pointss, pointss[0].slopeOrder());
        System.out.println("Comparator: ");
        for (int i=0 ; i<pointss.length; i++){
            System.out.println(pointss[i].toString());
        }
    } */
}

